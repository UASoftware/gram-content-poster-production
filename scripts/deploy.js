const fs = require('fs');
const path = require('path');
const Curl = require('curl-request');

const REMOTE_ARTIFACTS_URL = `https://api.bitbucket.org/2.0/repositories/${process.env.BITBUCKET_REPO_OWNER}/${process.env.BITBUCKET_REPO_SLUG}/downloads`;

const ARTIFACTS_FOLDER = './artifacts';

const deployFiles = async (filenames) => {
  const curl = new Curl();
  const {
    auth: { BASIC },
    option: { HTTPAUTH, USERNAME, PASSWORD },
  } = curl.libcurl;

  const headers = ['Content-Type: multipart/form-data'];

  const body = filenames.map((filename) => ({
    name: 'files',
    file: path.join(ARTIFACTS_FOLDER, filename),
  }));

  return curl
    .setOpt(HTTPAUTH, BASIC)
    .setOpt(USERNAME, process.env.BITBUCKET_USERNAME)
    .setOpt(PASSWORD, process.env.BITBUCKET_APP_PASSWORD)
    .setHeaders(headers)
    .setMultipartBody(body)
    .post(REMOTE_ARTIFACTS_URL);
};

const artifactsPath = path.resolve(process.cwd(), ARTIFACTS_FOLDER);

const files = fs.readdirSync(artifactsPath);

(async () => {
  try {
    await deployFiles(files);
    console.log('Artifacts deployed successfully!');
  } catch (err) {
    console.error(JSON.parse(JSON.stringify({ err })));
  }
})();
